 **
###  版本更新说明：
** 

20180913

        1、增加table级别的heightRemove参数，用法在例子上面有，可自行查看
            ,heightRemove:[".calss","#id",10]//不计算的高度,表格设定的是固定高度，此项不生效

201800912

        1、当前版本定为v0.1
        2、只依赖treeGrid.js（去掉dltable.js和treeGrid.css依赖）

20180830

        1、规范内部表格级参数与列级参数（全部列级别的参数需以lay_开头，请务必注意）

----------------------------------------------------------------------------------


**
### 基于layui的树表格-treeGrid  
** 

 **在线[demo](http://beijiyi.com/)【此服务器配置不是一般低，请耐心等待片刻，谢谢！】** 


 **效果图：** 
![输入图片说明](https://images.gitee.com/uploads/images/2018/0828/202603_1fa14869_1613602.png "屏幕截图.png")

折叠记忆

![折叠记忆](https://images.gitee.com/uploads/images/2018/0829/235912_ac99c8dd_1613602.gif "折叠记忆：.gif")

新增节点- 

![新增节点](https://images.gitee.com/uploads/images/2018/0829/235943_e2a0991e_1613602.gif "新增节点：.gif")

删除节点

![删除节点](https://images.gitee.com/uploads/images/2018/0829/235954_65ee1063_1613602.gif "删除节点：.gif")

删除多个节点

![删除多个节点](https://images.gitee.com/uploads/images/2018/0830/000003_0697a33b_1613602.gif "删除多个节点：.gif")

节点初始化隐藏、显示控制1

    1设置
        {"id":"111", "pId":"1", "name":"苹果","lay_is_open":false}        

节点初始化隐藏、显示控制2

![节点初始化隐藏、显示控制2](https://images.gitee.com/uploads/images/2018/0830/000022_438dc749_1613602.jpeg "节点初始化隐藏、显示控制2.jpg")

多选操作（选中父节点，子节点被选中；全部子节点被选中，父节点也被选中；自己看效果）

![多选操作](https://images.gitee.com/uploads/images/2018/0830/000031_fa1c48c1_1613602.gif "多选操作（选中父节点，子节点被选中；全部子节点被选中，父节点也被选中；自己看效果）.gif")

 **调用例子：** 
```
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>treeGrid</title>
    <link rel="stylesheet" href="design/css/layui.css">
    <script src="design/layui.js"></script>
</head>
<style>
    html,body{
        height: 100%;
    }
    div{
        -moz-box-sizing: border-box;  /*Firefox3.5+*/
        -webkit-box-sizing: border-box; /*Safari3.2+*/
        -o-box-sizing: border-box; /*Opera9.6*/
        -ms-box-sizing: border-box; /*IE8*/
        box-sizing: border-box; /*W3C标准(IE9+，Safari5.1+,Chrome10.0+,Opera10.6+都符合box-sizing的w3c标准语法)*/
    }
</style>
<body class="layui-layout-body" style="padding: 10px">
<div>
    <a class="layui-btn layui-btn-primary"  onclick="window.location.href='index.html';">刷新</a>
    <a class="layui-btn layui-btn-primary"  onclick="add(null);">新增一行</a>
    <a class="layui-btn layui-btn-primary"  onclick="openorclose();">隐藏或打开香蕉节点</a>
    <a class="layui-btn layui-btn-primary"  onclick="getCheckData();">获取选中行数据</a>
    <a class="layui-btn layui-btn-primary"  onclick="getCheckLength();">获取选中数目</a>
    <a class="layui-btn layui-btn-primary"  onclick="print();">打印缓存对象</a>
</div>
<div style="height: 100%">
    <table class="layui-hidden" id="treeTable" lay-filter="treeTable"></table>
</div>
<script>
    var editObj=null,ptable=null,treeGrid=null,tableId='treeTable',layer=null;
    layui.config({
        base: 'design/extend/'
    }).extend({
        treeGrid:'treeGrid'
    }).use(['jquery','treeGrid','layer'], function(){
        var $=layui.jquery;
        treeGrid = layui.treeGrid;//很重要
        layer=layui.layer;
        ptable=treeGrid.render({
            id:tableId
            ,elem: '#'+tableId
            ,idField:'id'
            ,url:'/json/treeData.json'
            ,cellMinWidth: 100
            ,treeId:'id'//树形id字段名称
            ,treeUpId:'pId'//树形父id字段名称
            ,treeShowName:'name'//以树形式显示的字段
            ,height:'full-60'
            ,cols: [[
                {type:'numbers'}
                ,{type:'checkbox'}
                ,{width:100,title: '操作', align:'center'/*toolbar: '#barDemo'*/
                    ,templet: function(d){
                    var html='';
                    var addBtn='<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="add">添加</a>';
                    var delBtn='<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>';
                    return addBtn+delBtn;
                }
                }
                ,{field:'name', edit:'text',width:300, title: '水果名称'}
                ,{field:'id',width:100, title: 'id'}
                ,{field:'pId', title: 'pid'}
            ]]
            ,page:false
        });

        treeGrid.on('tool('+tableId+')',function (obj) {
            if(obj.event === 'del'){//删除行
                del(obj);
            }else if(obj.event==="add"){//添加行
                add(obj);
            }
        });
    });

    function del(obj) {
        layer.confirm("你确定删除数据吗？如果存在下级节点则一并删除，此操作不能撤销！", {icon: 3, title:'提示'},
            function(index){//确定回调
                obj.del();
                layer.close(index);
            },function (index) {//取消回调
               layer.close(index);
            }
        );
    }


    var i=1000000;
    //添加
    function add(pObj) {
        var pdata=pObj?pObj.data:null;
        console.log(pdata);
        var param={};
        param.name='水果'+Math.random();
        param.id=++i;
        param.pId=pdata?pdata.id:null;
        treeGrid.addRow(tableId,pdata?pdata[treeGrid.config.indexName]+1:0,param);
    }

    function print() {
        console.log(treeGrid.cache[tableId]);
        var loadIndex=layer.msg("对象已打印，按F12，在控制台查看！", {
            time:3000
            ,offset: 'auto'//顶部
            ,shade: 0
        });
    }
    
    function openorclose() {
        var map=treeGrid.getDataMap(tableId);
        var o= map['102'];
        treeGrid.treeNodeOpen(o,!o[treeGrid.config.cols.isOpen]);
    }

    function getCheckData() {
        var checkStatus = treeGrid.checkStatus(tableId)
            ,data = checkStatus.data;
        layer.alert(JSON.stringify(data));
    }
    function getCheckLength() {
        var checkStatus = treeGrid.checkStatus(tableId)
            ,data = checkStatus.data;
        layer.msg('选中了：'+ data.length + ' 个');
    }
</script>
</body>
</html>
```



```
{
    "msg":"","code":0,
    "data":[
        {"id":"1", "pId":"0", "name":"水果"},
        {"id":"101", "pId":"1", "name":"苹果"},
        {"id":"102", "pId":"1", "name":"香蕉"},
        {"id":"103", "pId":"1", "name":"梨"},
        {"id":"10101", "pId":"101", "name":"红富士苹果"},
        {"id":"10102", "pId":"101", "name":"红星苹果"},
        {"id":"10103", "pId":"101", "name":"嘎拉"},
        {"id":"10104", "pId":"101", "name":"桑萨"},
        {"id":"10201", "pId":"102", "name":"千层蕉"},
        {"id":"10202", "pId":"102", "name":"仙人蕉"},
        {"id":"10203", "pId":"102", "name":"吕宋蕉"}
    ]
    ,"count":29
}
```


例子文件路径：
    testTreeGrid/web/index.html


 **使用说明：** 
